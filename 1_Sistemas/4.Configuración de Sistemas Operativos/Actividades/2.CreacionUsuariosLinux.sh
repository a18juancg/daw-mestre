#!/bin/bash

#crea un fichero de datos de usuarios con el siguiente formato
#identifcador:nombre_user:nombre:apellidos:email:pass

#el fichero se lo vamos a pasar como parámetro al ejecutar el script
#comprobamos que el fichero existe
if  [ -f $1 ]
then
	echo "se va a leer el archivo de usuarios "$1
else
	echo "no ha indicado ningún archivo al ejecutar el script"
	exit 1
fi

#creamos los usuarios
#Guarda en una variable datos el contenido del fichero
...............

#Para cada linea del fichero extraemos cada campo que necesitemos como parametro del comando useradd
for i in $datos
do
echo "Linea del fichero: "$i
#p.ej. para extraer el login del usuario
login=`echo $i|cut -d: -f2`

#completa leyendo el resto de campos que vayas a necesitar para crear el usuario
...........
...........
...........

#ahora ejecuta el comando useradd con las opciones correspondientes a los parámetros extraidos en el paso anterior
.....................

sleep 1
echo "\n Uuario $login creado"

done
