/************* EJERCICIO 1 ***************/
drop database if exists ejercicios;
create database ejercicios;
use ejercicios;

create table cocktel(
	nombre varchar(50) primary key,
    descripcion varchar(200) not null
);

create table ingrediente(
	nombre varchar(50) primary key,
    foto blob,
    calorias decimal(6,2)
);

create table receta(
	nomcocktel varchar(50),
    nomingrediente varchar(50),
    constraint PK_receta primary key (nomcocktel,nomingrediente),
    constraint FK_receta_cocktel foreign key (nomcocktel) 
		references cocktel(nombre) on delete restrict on update cascade,
    constraint FK_receta_ingrediente foreign key (nomingrediente) 
		references ingrediente (nombre) on delete restrict on update cascade
);

/************* EJERCICIO 2 ***************/
drop database if exists ejercicios;
create database ejercicios;
use ejercicios;

create table comunidad(
	idcom tinyint unsigned primary key,
    nombre varchar(50) unique not null,
    codcapital mediumint unsigned
);

create table provincia(
	codprov tinyint unsigned primary key,
    nombre varchar(50) unique not null,
    superficie decimal(8,2),
    idcom tinyint unsigned not null,
    codcapital mediumint unsigned,
    constraint FK_prov_com foreign key (idcom)
		references comunidad(idcom) on delete restrict on update cascade
);

create table localidad(
	idloc mediumint unsigned primary key,
    poblacion mediumint unsigned not null,
    nombre varchar(100) unique not null,
    codprov tinyint unsigned,
    constraint FK_loc_prov foreign key (codprov) 
		references provincia(codprov) on update cascade on delete restrict
);

alter table comunidad add constraint FK_com_capital foreign key (codcapital) 
	references localidad(idloc) on delete set null on update cascade;
alter table provincia add constraint FK_prov_capital foreign key (codcapital) 
	references localidad(idloc) on delete set null on update cascade;

-- Crea una vista “V_PROVINCIAS_GAL” que muestre únicamente las provincias de la comunidad 12.
create or replace view v_provincias_gal as 
	select * from provincia where idcom=12;
-- Crea una nueva vista que muestre solamente nombre y población de las localidades de la provincia 15.
create or replace view v_localidades_prov15 as
	select nombre, poblacion from localidad where codprov=15;
/* Modifica la tabla localidad para poder guardar la superficie de cada localidad en metros cuadrados, 
 	con una precisión de 2 decimales. */
alter table localidad add superficie decimal(8,2);
-- Elimina el campo superficie de la tabla de provincias.
alter table provincia drop superficie;
/* Añade una tabla “PAIS” con su código, nombre e idioma (que solo podrá ser “inglés”, “español”,
	“francés” u “otro”). El nombre del país no podrá estar repetido y tendrá que ser indicado. 
    El código del país se asignará automáticamente.*/
create table pais(
	codigo smallint unsigned auto_increment primary key,
	nombre varchar(50) unique not null,
    idioma enum ('ingles','español','frances','otros')
);
-- Añade a la tabla comunidad su código de país, con su consiguiente clave foránea.
alter table comunidad add (
	codpais smallint unsigned not null,
    constraint FK_com_pais foreign key (codpais) references pais(codigo)
);

/* OTRA OPCION: hacerlo por partes 
alter table comunidad add codpais smallint unsigned not  null;
alter table comunidad add constraint FK_com_pais foreign key (codpais) references pais(codigo);
*/


/************* EJERCICIO 3 ***************/
drop database if exists ejercicios;
create database ejercicios;
use ejercicios;

create table Categoria(
	codcat tinyint unsigned primary key,
    categoria varchar(50) not null
    );
    
create table Departamento(
	coddepto tinyint unsigned primary key,
    nombre varchar(50) not null,
    codcoordinador smallint unsigned
    );
	
create table Empleado(
	codemp smallint unsigned primary key,
    dni char(9) not null unique,
    num_ss char(16) not null unique,
    direccion varchar(150) not null,
    nombre varchar(50) not null,
    apellidos varchar(50) not null,
    codjefe smallint unsigned,
    coddepto tinyint unsigned default 0,
		constraint FK_empleado_departamento foreign key (coddepto) 
			references departamento (coddepto) on update cascade on delete cascade, -- on delete set default 
        constraint FK_empleado_jefe foreign key (codjefe) references empleado (codemp)
			on update cascade on delete set null
);
   
alter table Departamento add constraint FK_depart_empleado foreign key (codcoordinador) 
	references empleado (codemp) on update cascade on delete set null;

create table telf_emp(
	codemp smallint unsigned,
    numero char(12),
		constraint PK_codemp_numero primary key (codemp, numero),
        constraint FK_telf_empleado foreign key (codemp) references empleado (codemp)
			on update cascade on delete cascade
);

create table contrato(
	codemp smallint unsigned,
    fecini date,
    fecfin date not null,
    codcat tinyint unsigned not null default 1,
		constraint FK_contrato_categoria foreign key (codcat)
			references categoria (codcat) on update cascade on delete restrict, -- set default
		constraint PK_contrato primary key (codemp,fecini),
        constraint FK_contrato_empleado foreign key (codemp) 
			references empleado (codemp) on update cascade on delete restrict
);

create table nominas(
	codemp smallint unsigned,
    fecini date,
    fecnomina date,
    salario  decimal (9,2) not null,
		constraint PK_nominas primary key (codemp,fecini,fecnomina),
       constraint FK_nominas_contrato foreign key (codemp,fecini) 
			references contrato (codemp,fecini) on update restrict on delete restrict
);

create index idx_nomapel_emp on empleado(nombre, apellidos);

/************* EJERCICIO 4 ***************/
drop database if exists ejercicios;
create database ejercicios;
use ejercicios;

create table Empleado (
	codemp smallint unsigned auto_increment primary key, 
	nombre varchar(50) not null, 
    apellido1 varchar(100) not null, 
    apellido2 varchar(100), 
    direccion varchar(150), 
    provincia varchar(30) default 'Pontevedra', 
    dni char(9) unique, 
    coddepto tinyint unsigned, 
    salario decimal(7,2) default 800, 
    fecalta char(10),  
    dto decimal(5,2)
);

create table Departamento (
	coddepto tinyint unsigned auto_increment primary key,  
    depto varchar(50) not null, 
    codjefe smallint unsigned,  
    presupuesto decimal(10,2) default 0,
    constraint FK_depto_jefe foreign key (codjefe) references empleado(codemp)
);

alter table empleado add constraint FK_emp_depto foreign key (coddepto) references departamento(coddepto)
	on delete restrict on update cascade;

create table Horas_extra (
	codemp smallint unsigned, 
	fecha date, 
    num_horas tinyint unsigned not null,
    constraint PK_horas_extra primary key (codemp,fecha),
    constraint FK_horas_emp foreign key (codemp) references empleado(codemp)
);

-- Añade un campo a la tabla empleado para guardar el importe que cobra el empleado por sus horas extras.
alter table empleado add importe_horas decimal(4,2);
/* Todos los empleados del mismo departamento cobran lo mismo, así que mejor elimina el campo
que acabas de crear y añádelo a la tabla departamento. */
alter table empleado drop importe_horas;
alter table departamento add importe_horas decimal(4,2);
-- Cambia el nombre del campo Empleado.dto por Empleado.descuento y del campo Departamento.depto por Departamento.departamento
alter table empleado change dto descuento decimal(5,2);
alter table departamento change depto departamento varchar(50) not null;
/* Se han dado cuenta que guardar la fecha de alta del empleado como texto es una tontería, así que
la cambiaremos para que sea una fecha. Además, debe tomar por defecto el valor '2019-01-21' .*/
alter table empleado modify fecalta date default '2019-01-21';
-- Modifica el campo dni del empleado para que tenga que ser indicado obligatoriamente.
alter table empleado modify dni char(9) unique not null;
-- Crea una vista que muestre toda la información de los empleados del departamento 4.
create view v_emp_depto4 as select * from empleado where coddepto=4;
-- Crea una vista que muestre únicamente el código, nombre y apellidos de todos los empleados.
create view v_resumen_emp as select codemp,nombre,apellido1,apellido2 from empleado;
/* Crea una vista que muestre el código de cada empleado junto con las horas extra que ha trabajado (no
hace falta que salgan sumadas, pueden salir fila a fila). */
create view v_horas as select codemp,num_horas from Horas_extra;
-- Crea un índice en el campo Empleado.dni
create index idx_dni_emp on empleado(dni);
-- Crea un índice único en el campo Departamento.departamento.
create unique index idx_depto_depto on departamento(departamento);
-- Crea un índice en la tabla Horas_extras en los campos fecha y codemp (en ese orden).
create index idx_horas_feccodemp on horas_extra(fecha,codemp);
-- Elimina el índice del campo empleado.dni.
drop index idx_dni_emp on empleado;

/************* EJERCICIO 5 ***************/
drop database if exists ejercicios;
create database ejercicios;
use ejercicios;

create table TFNO_PROFE(
	codprof smallint unsigned,
    numero bigint,
    principal boolean,
    constraint PF_tfnos primary key(codprof,numero)
);

create table PROFESOR(
	codprof smallint unsigned auto_increment primary key,
	dni char(9) unique not null,
    nombre varchar(50) not null,
    direccion varchar(150),
    tipocarnet set('A1', 'A2', 'B1', 'B2') 
);

alter table tfno_profe add constraint fk_tfno_profe foreign key (codprof) references profesor(codprof)
	on delete cascade on update cascade;

create table MODULO(
	codmod smallint unsigned auto_increment primary key,
    nombre varchar(100) not null,
    codprof smallint unsigned,
    coddelegado int unsigned,
    ciclo enum('SMIR', 'ASIR', 'DAM', 'DAW') not null,
    constraint FK_mod_prof foreign key (codprof) references profesor(codprof)
		on delete set null on update cascade
);

create table MATRICULA(
	codmod smallint unsigned,
    codalumno int unsigned,
    fecmatricula timestamp not null,
    constraint PK_matricula primary key (codmod,codalumno),
    constraint FK_matricula_mod foreign key (codmod) references modulo(codmod)
		on delete restrict on update cascade
);

create table ALUMNO(
	numexp int unsigned auto_increment primary key,
    nombre varchar(50) not null,
    apellidos varchar(100) not null,
    fecnac date
);

alter table modulo add constraint FK_modulo_alumno foreign key (coddelegado) references alumno(numexp)
	on delete set null on update cascade;
alter table matricula add constraint FK_matricula_alumno foreign key (codalumno) references alumno(numexp)
	on delete restrict on update cascade;
    
-- Añade los campos “apellido1” y “apellido2” a la tabla PROFESOR.
alter table profesor add apellido1 varchar(100) not null;
alter table profesor add apellido2 varchar(100);
-- Modifica el campo “fecnac” de la tabla ALUMNO para que pase a llamarse “fechanac”
alter table alumno change fecnac fechanac date;
-- nos han pedido que el número de teléfono del profesor lo guardemos como un texto en lugar de como número.
alter table tfno_profe modify  numero char(12);
-- Cambia el nombre de la tabla TFNO_PROFE from TELEFONOS
alter table tfno_profe rename to telefonos;
-- Elimina la FK que tiene el campo “coddelegado”
alter table modulo drop foreign key FK_modulo_alumno;
-- Añade en la tabla MODULO un campo llamado “seguimiento”, que será un número entero de tamaño máximo 999.999
alter table modulo add seguimiento mediumint unsigned;
-- Modifica el campo MODULO.codprof para que no pueda ser nulo.
alter table modulo modify codprof smallint unsigned not null; -- peta porque tengo una FK con set null
/* Nos interesa emplear la tabla de TFNO_PROFE para guardar teléfonos también de los alumnos, por lo que necesitamos
añadir un nuevo campo a la tabla que indique el tipo de teléfono (podrá ser ‘ALUMNO’ o ‘PROFESOR’), y eliminar la foreign key que teníamos en el campo codprof.*/
alter table telefonos add tipotelefono enum('ALUMNO','PROFESOR') not null;
alter table telefonos drop foreign key fk_tfno_profe;
-- Crea las siguientes vistas:
/* Desde la administración del centro nos solicitan consultar el número de expediente que corresponde a cada alumno, por lo que les crearemos un usuario 
para que puedan acceder a la BD y a ese usuario le permitiremos consultar el número de expediente, nombre y apellidos de los alumnos. */
create view v_alumnos as select numexp,nombre,apellidos from alumno;
/* Desde conserjería nos piden también poder consultar los teléfonos de los profesores, por lo que les creamos un usuario que pueda acceder a los datos que 
devuelve la consulta: select p.nombre,t.numero from profesor p, tfno_profe t where p.codprof=t.codprof */
create view v_profes as select p.nombre,t.numero from profesor p, telefonos t where p.codprof=t.codprof;
/* Con el nombre solo no se aclaran, así que nos piden que modifiquemos la vista anterior para incluir también el primer apellido del profesor 
(select p.nombre, p.apellido1,t.numero from profesor p,tfno_profe t where p.codprof=t.codprof) */
create or replace view v_profes as select p.nombre, p.apellido1,t.numero from profesor p,telefonos t where p.codprof=t.codprof;
-- Crea los índices necesarios para garantizar que el rendimiento sea óptimo, teniendo en cuenta que las operaciones más habituales que se realizan contra la BD son las siguientes:
-- A partir del apellido de un profesor, buscamos el código que le corresponde.
create index idx_prof_apel on profesor(apellido1,apellido2);
-- Con ese código, buscamos su(s) número(s) de teléfono y los códigos de los módulos que imparte.
create index idx_tfno_prof on telefonos(codprof);
create index idx_mod_prof on modulo(codprof);
-- A partir del código de los módulos, buscamos qué códigos de alumno están matriculados en ese módulo.
create index idx_matr_mod on matricula(codmod);
-- Buscar el número de expediente de un alumno a partir de su nombre y apellidos.
create index idx_alumno_nomapel on alumno(nombre, apellidos);
-- Añade un índice en el campo MODULO.seguimiento que impida que haya valores repetidos en dicho campo.
create unique index idx_mod_segu on modulo(seguimiento);
-- Hemos visto que tras crear los índices el rendimiento se ha visto penalizado. Elimina los dos primeros índices creados anteriormente.
drop index idx_prof_apel on profesor;
drop index idx_tfno_prof on telefonos;

/************* EJERCICIO 6 ***************/
drop database if exists ejercicios;
create database ejercicios;
use ejercicios;

create table actores(
	codigo smallint unsigned auto_increment primary key,
    nombre varchar(100) not null,
    fecha date not null,
    nacionalidad varchar(20) default 'EEUU'
);

create table personajes(
	codigo mediumint unsigned auto_increment primary key,
    nombre varchar(50) not null,
    raza varchar(20) not null,
    grado varchar(30) not null,
    codigoactor smallint unsigned not null,
    codigosuperior mediumint unsigned,
    constraint FK_pers_actor foreign key (codigoactor) references actores(codigo) on delete restrict on update cascade,
    constraint FK_pers_pers foreign key (codigosuperior) references personajes(codigo) on delete restrict on update cascade
);

create table planetas(
	codigo serial primary key,
    galaxia enum('VIA_LACTEA','ANDRÓMEDA','BARNARD','BODE','DE_LOS_OJOS') not null,
    nombre varchar(50) not null
);

create table capitulos(
	temporada tinyint unsigned,
    orden tinyint unsigned zerofill check (orden<21),
    titulo varchar(30) not null,
    fecha date not null,
    constraint PK_capitulos primary key(temporada,orden)
);

create table peliculas(
	codigo tinyint unsigned auto_increment primary key,
    titulo varchar(50) not null,
    director varchar(200) not null,
    anho year
);

create table pers_capitulos(
	codpers mediumint unsigned,
    temporada tinyint unsigned,
    orden tinyint unsigned, -- no hace falta el zerofill
    constraint PK_pers_capitulos primary key (codpers,temporada,orden),
    constraint FK_perscap_pers foreign key (codpers) references personajes(codigo) on delete restrict on update cascade,
    constraint FK_perscap_cap foreign key (temporada,orden) references capitulos (temporada,orden) on delete restrict on update cascade
);

create table pers_peliculas(
	codpers mediumint unsigned,
    codpeli tinyint unsigned,
    constraint PK_pers_peliculas primary key (codpers,codpeli),
    constraint FK_perspeli_pers foreign key (codpers) references personajes (codigo) on delete restrict on update cascade,
    constraint FK_perspeli_peli foreign key (codpeli) references peliculas (codigo) on delete restrict on update cascade
);

create table visitas(
	codnave smallint unsigned,
    codplaneta bigint unsigned, -- OJO: no puede ser serial porque seria auto_increment 
    temporada tinyint unsigned,
    orden tinyint unsigned, -- tampoco hace falta el zerofill
    constraint PK_visitas primary key (codnave,codplaneta,temporada,orden),
    constraint FK_visitas_planeta foreign key (codplaneta) references planetas(codigo) on delete restrict on update cascade,
    constraint FK_visitas_capitulo foreign key (temporada, orden) references capitulos (temporada,orden) on delete restrict on update cascade
);

create table naves(
	codigo smallint unsigned auto_increment primary key,
    numtripulantes tinyint unsigned,
    nombre varchar(50) not null
);

alter table visitas add constraint FK_visitas_nave foreign key (codnave) references naves(codigo) on delete restrict on update cascade;

-- Añade un campo en la tabla PERSONAJES_PELICULAS que indique si el papel es principal o no.
alter table pers_peliculas add principal boolean;
-- Modifica la tabla CAPITULOS para que el campo fecha pase a guardar la hora de publicación con fecha.
alter table capitulos modify fecha timestamp not null;
-- Deshaz el cambio y deja el campo como estaba.
alter table capitulos modify fecha date not null;
-- Para asegurar que no se puedan publicar varios capítulos simultáneamente, impide que se puedan insertar valores duplicados en ese campo.
alter table capitulos modify fecha date unique not null;
-- Para cada personaje queremos guardar una fotografía, por lo que necesitamos añadir un campo “foto” a la tabla.
alter table personajes add foto mediumblob;
-- Queremos guardar también el tipo de propulsor que usa cada nave, que puede ser uno de entre: ‘NUCLEAR’,’REACCION’,’FLUJO’ o ’MAGNETICO’. 
-- Las naves pueden usar más de un propulsor, que pueden ser de distintos tipos.
alter table naves add tipopropulsor set('NUCLEAR','REACCION','FLUJO' ,'MAGNETICO');
-- Cambia el nombre de todos los campos de tipo “codigoXXX” por “codXXX”.
-- select concat('alter table ',table_name,' change codigo cod',table_name,' ',data_type,' unsigned auto_increment;') from information_schema.columns where column_name='codigo';
alter table actores change codigo codactores smallint unsigned auto_increment;
alter table planetas change codigo codplanetas bigint unsigned auto_increment;
alter table peliculas change codigo codpeliculas tinyint unsigned auto_increment;
alter table personajes change codigo codpersonajes mediumint unsigned auto_increment;
alter table naves change codigo codnaves smallint unsigned auto_increment;
-- Añade un campo a la tabla CAPITULOS que indique si es final de temporada o no.
alter table capitulos add final boolean;
-- Queremos impedir que un director pueda publicar más de una película por año, por lo que esos campos no se podrán duplicar en la tabla de películas.
alter table peliculas add constraint UQ_anho_dir unique (anho,director);
-- Crea los siguientes índices:
-- Habitualmente se consulta la información tanto de los actores como de los personajes por su nombre, por lo que queremos optimizar ese tipo de consultas.
create index idx_actor_nb on actores(nombre);
create index idx_pers_nb on personajes(nombre);
-- Es muy habitual también consultar las películas por director y año.
create index idx_peli_diranho on peliculas(director,anho);
-- Define las siguientes vistas:
-- Crea una vista para mostrar únicamente los planetas de la vía láctea.
create or replace view v_planetas_via_lactea as select * from planetas where galaxia='VIA LACTEA';
-- Crea una vista “NAVES_GRANDES” que muestre las naves que tienen más de 10 tripulantes.
create or replace view v_naves_grandes as select * from naves where numtripulantes>10;
-- Modifica la vista anterior para que muestre también las naves de más de 8 tripulantes.
create or replace view v_naves_grandes as select * from naves where numtripulantes>8;