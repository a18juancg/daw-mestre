drop database if exists LG2019;
create database LG2019;
use LG2019;

create table Autores ( 
	codautor smallint  primary key,
	nombre varchar(50) not null,
	apellido1 varchar(50) not null,
	apellido2 varchar(50),
	alias varchar(50),
	fecnac date,
	fecdefunc date,
	anho_lg year not null,
	genero enum('home','muller') not null,
	edad_primera_obra tinyint unsigned not null,
	miembro_RAGL boolean,
	defunc_natural boolean,
	codlocnac smallint not null, 
	codlocdefunc smallint not null
);

create table Asociacion (
	codasoc smallint auto_increment primary key,
	asociacion varchar(100) not null,
	es_partido boolean,
	es_galleguista boolean
);

create table Residencias (
	codautor smallint ,
	codloc smallint,
	fecini date,
	fecfin date not null,
	constraint PK_residencias primary key (codautor,codloc,fecini)
);

create table Obras(
	codobra int primary key,
	fecha date,
	numobra tinyint,
	idioma varchar(30) not null,
	edad_autor tinyint unsigned,
	codautor smallint ,
	codtipoobra tinyint ,
	codtemaprincipal smallint,
	codeditorial smallint
);

create table TipoObra (
	codtipo tinyint auto_increment primary key,
	tipo varchar(80) not null
);

create table Influencias(
    joven boolean,
    codinfluyente smallint ,
    codinfluido smallint ,
	constraint PK_INFLUENCIAS primary key (codinfluyente,codinfluido)
);

 create table titulacion(
    nombre varchar(50),
    codtitulacion smallint auto_increment primary key
);

create table Profesion (
	codprof smallint auto_increment primary key,
	profesion varchar(30) not null
);

create table Profesion_autor (
	codprof smallint,
	codautor smallint,
	principal boolean,
	constraint pk_profesion_autor primary key (codprof, codautor)
);

create table miembros_asociaciones(
	codasoc smallint,
	codautor smallint,
	directivo boolean,
	constraint PK_miembros_asoc primary key (codasoc,codautor)
);

create table exilia(
	codautor smallint ,
	codpais tinyint ,
	fecha date,
	constraint pk_exilia primary key (codautor,codpais,fecha)
);

create table Pais(
    codpais tinyint primary key,
    pais varchar(50) not null,
    continente varchar(50) not null
);

create table Comunidad(
    codcom tinyint primary key,
    comunidad varchar(50) not null,
    codpais tinyint not null    
);

create table Provincia(
    codprov tinyint primary key,
    provincia varchar(50) not null,
    codcom tinyint not null
);

create table Localidad(
    codlocalidad smallint primary key,
    localidad varchar(100) not null,
    codprov tinyint not null
);

create table Editorial(
    codeditorial smallint primary key,
    nombre varchar(100) not null,
    procedencia varchar(50),
    codloc smallint not null
);

create table Tematica(
    codtema smallint primary key,
    tematica  varchar(100) not null,
    descripcion  varchar(500) not null
);

create table TipoPremio (
    codtipopremio tinyint primary key,
    tipopremio varchar(50)
);

create table Premio (
    codpremio smallint primary key,
    nombre varchar(50),
    organizacion varchar(50),
    codtipopremio tinyint    
);

create table Nominaciones (
    codobra int,
    codpremio smallint,
    galardonada boolean,
    fecha date not null,
    constraint PK_Nominaciones primary key (codobra, codpremio)
);

create table estudios(
    codautor smallint ,
    codtitulacion smallint,
	constraint pk_estudios primary key (codautor,codtitulacion)
);

alter table estudios add constraint FK_estudios_autores foreign key (codautor) references autores(codautor);
alter table estudios add constraint FK_estudios_titulacion foreign key (codtitulacion) references titulacion(codtitulacion);
alter table nominaciones add constraint FK_premio foreign key (codpremio) references Premio(codpremio) on delete restrict on update cascade;
alter table nominaciones add constraint FK_obra foreign key (codobra) references Obras(codobra) on delete restrict on update cascade;
alter table Obras add constraint FK_obras_tipoobra foreign key (codtipoobra) references TipoObra(codtipo) on delete restrict on update cascade;
alter table Obras add constraint FK_obras_autor foreign key (codautor) references Autores(codautor) on delete restrict on update cascade;
alter table Obras add constraint FK_obras_tema foreign key (codtemaprincipal) references Tematica(codtema) on delete restrict on update cascade;
alter table Obras add constraint FK_obras_editorial foreign key (codeditorial) references Editorial(codeditorial) on delete restrict on update cascade;
alter table Autores add constraint fk_locnac foreign key (codlocnac) references Localidad(codlocalidad) on delete restrict on update cascade;
alter table Autores add constraint fk_locdefunc foreign key (codlocdefunc) references Localidad(codlocalidad) on delete restrict on update cascade;
alter table Residencias add constraint fk_codautor foreign key (codautor) references Autores(codautor) on delete restrict on update cascade;
alter table Residencias add constraint fk_codloc foreign key (codloc) references Localidad(codlocalidad) on delete restrict on update cascade;
alter table Profesion_autor add constraint fk_profesorautor_profesion foreign key (codprof) references Profesion(codprof);
alter table Profesion_autor add constraint fk_profesorautor_autor foreign key (codautor) references Autores(codautor);
alter table influencias add constraint FK_influencias_influye foreign key (codinfluyente) references autores(codautor) on delete restrict on update cascade;
alter table influencias add constraint FK_influencias_influido foreign key (codinfluido) references autores (codautor) on delete restrict on update cascade;
alter table miembros_asociaciones add constraint fk_miem_asoci FOREIGN KEY (codasoc) references asociacion (codasoc) on delete restrict on update cascade;
alter table miembros_asociaciones add constraint fk_miem_autor FOREIGN KEY (codautor) references autores (codautor) on delete restrict on update cascade;
alter table exilia add constraint fk_exilia_autor FOREIGN KEY (codautor) references autores (codautor) on delete restrict on update cascade;
alter table exilia add constraint fk_exilia_pais FOREIGN KEY (codpais) references pais (codpais) on delete restrict on update cascade;
alter table comunidad add constraint FK_pais_com foreign key (codpais) references Pais(codpais) on delete restrict on update cascade;
alter table provincia add constraint FK_com_prov foreign key (codcom) references Comunidad(codcom) on delete restrict on update cascade;
alter table localidad add constraint FK_prov_loc foreign key (codprov) references Provincia(codprov) on delete restrict on update cascade;
alter table editorial add constraint FK_loc_edi foreign key (codloc) references Localidad(codlocalidad) on delete restrict on update cascade;
alter table premio add constraint FK_tipopremio foreign key (codtipopremio) references TipoPremio(codtipopremio) on delete restrict on update cascade;
