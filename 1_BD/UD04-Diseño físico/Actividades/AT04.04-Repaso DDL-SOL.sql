drop database if exists AT0403;
Create database AT0403;
use AT0403;

Create table CLIENTE(
	Codcliente int(6) auto_increment primary key,
	Nombre varchar(25) not null,
	Apellidos varchar(100) not null,
	Dni char(9) unique,
	Direccion varchar(50),
	Codpostal int(5),
	Localidad varchar(25) default 'SANTIAGO',
	Fax varchar(11),
	Descuento decimal(4,2) default 0,
	Zonaventas int(2) not null 
);

Create table ZONA(
	Zona int(2) auto_increment primary key,
	Descripcion varchar(25) not null
);

alter table CLIENTE add constraint FK_CLIENTE_ZONA foreign key (zonaventas) references ZONA(zona) on delete restrict on update cascade;

Create table PRODUCTO(
	Codprod int(6) auto_increment primary key,
	Nombre varchar(20) not null,
	Descripcion varchar(30),
	Pvp decimal(10,2),
	Iva decimal (3,2) default 0.21,
	Foto blob
);

Create table PEDIDO(
	Numpedido int(10) unsigned auto_increment primary key,
	Codcliente int(6) not null,
	Fecpedido timestamp not null,
	fecpago date,
	codtipopedido int(2) not null,
	Forma_pago enum('EFECTIVO','TARJETA DEBITO','TARJETA CREDITO','TRANSFERENCIA')not null,
	Enviado boolean default false,
	Descuento decimal(4,2) default 0,
	constraint FK_pedido_cliente foreign key (codcliente) references cliente(codcliente) on update cascade on delete restrict
);

Create table lineapedido(
	Numpedido int(10) unsigned,
	Numlinea int(6) unsigned,
	Codproducto  int(6) not null,
	Unidades int(3) default 1,
	Constraint PK_LINPEDIDO primary key(numpedido,numlinea),
	constraint FK_linpedido_codpedido foreign key(numpedido) references pedido(numpedido) on delete restrict on update cascade,
	constraint FK_linpedido_prod foreign key (codproducto) references producto(codprod) on delete restrict on update cascade
);

Create table tipopedido(
	Codtipopedido int(2) primary key,
	Tipopedido varchar(100) unique not null
);

alter table PEDIDO add constraint FK_pedido_tipoped foreign key(codtipopedido) references tipopedido(codtipopedido) on delete restrict on update cascade;

--	Necesitamos limitar el acceso para el personal de administraci�n de modo que solo puedan consultar la siguiente informaci�n de los clientes: Nombre, Apellidos, DNI, Direccion, Codpostal, Localidad y Descuento. 
create view v_cliente_admin as select Nombre, Apellidos, DNI, Direccion, Codpostal, Localidad, Descuento from cliente;
--	El personal de ventas podr� seguir viendo toda la informaci�n de los clientes excepto el descuento que se les aplica, y toda la informaci�n de los productos excepto el pvp. 
create view v_cliente_ventas as select Codcliente,Nombre, Apellidos, Dni, Direccion, Codpostal, Localidad, Fax, Zonaventas from cliente;
create view v_producto_ventas as select Codprod, Nombre, Descripcion, Iva, Foto from producto;
--	Siempre que consultamos un cliente, lo hacemos por su nombre y apellidos o DNI, por lo que nos interesar�a que estas consultas se ejecuten lo m�s r�pidamente posible.
create index idx_cliente_nb_apel on cliente (nombre, apellidos);
--	Cuando buscamos un producto en la BD, lo hacemos siempre por su nombre, por lo que tambi�n queremos que las consultas de nombres de productos sean muy r�pidas.
create index idx_producto_nombre on producto (nombre);
--	-	Al finalizar el d�a, se elabora un informe de los pedidos pagados ese d�a, por lo que queremos que las b�squedas por la fecha de pago sean lo m�s r�pidas posible
create index idx_pedido_fecha on pedido(fecpago);

--	Los apellidos de los clientes nos interesa guardarlos por separado, por lo que queremos tener 2 campos en lugar de uno. Los campos se llamar�n apellido1 y apellido2, y ser�n textos de longitud 50.El primero no podr� ser nulo, pero el segundo s�.
alter table cliente add apellido2 varchar(50);
alter table cliente change apellidos apellido1 varchar(50) not null;
--	Cambia el nombre del campo codproducto de la tabla LINEAPEDIDO para que pase a llamarse codprod.
alter table lineapedido change codproducto codprod int(6) not null;
--	Cambia el nombre de la tabla LINEAPEDIDO por LINPEDIDO.
alter table lineapedido rename to linpedido;
--	La longitud del campo �nombre� del cliente es muy peque�a, c�mbiala por una longitud 50.
alter table cliente modify nombre varchar(50) not null;
--	Hemos creado un campo �fax� para el cliente pero no el tel�fono. A�ade un campo para guardar el tel�fono (se guardar� como un n�mero de longitud 11).
alter table cliente add telefono int(11);
--	Incrementa el tama�o del campo �unidades� de la tabla �lineapedido� para que tenga 5 d�gitos.
alter table linpedido modify unidades int(5);
--	Borra la tabla �tipopedido�, que no la estamos usando para nada.
alter table pedido drop foreign key FK_pedido_tipoped;
alter table pedido drop codtipopedido;
drop table tipopedido;
--	Elimina el campo �foto� de la tabla �Producto�.
alter table producto drop foto;
--	Las zonas apenas se modifican, as� que vamos a modificar el tipo de almacenamiento de la tabla para que sea de tipo �MyISAM�.
alter table cliente drop foreign key FK_CLIENTE_ZONA;
alter table zona engine='MyISAM';
