DROP SCHEMA IF EXISTS PRUEBAS;
CREATE DATABASE pruebas;

USE pruebas;

/* TABLAS */
CREATE TABLE departamentos(
	coddepto int auto_increment primary key,
	departamento varchar(50) unique not null,
	codjefe int
);

CREATE TABLE empleados ( 
	codigo  INT AUTO_INCREMENT PRIMARY KEY,
	nombre VARCHAR (40) not null,
	apellidos varchar(100),
	comision INTEGER 
);

CREATE TABLE proyectos(
	codproy int auto_increment primary key,
	presupuesto decimal(10,2) default 0,
	codgerente int references empleados(codigo) -- defino la foreign key a nivel de columna
);
select * from information_schema.REFERENTIAL_CONSTRAINTS where constraint_schema='pruebas' and table_name like 'proyectos'; -- Muestra las foreign keys ... y no está la que acabamos de crear!!
insert into proyectos(presupuesto, codgerente) values (5000,1); -- Insertamos un valor para el codgerente que no existe en la tabla de empleados (que está vacía)... y permite hacerlo!!
drop table proyectos;
CREATE TABLE proyectos(
	codproy int auto_increment primary key,
	presupuesto decimal(10,2) default 0,
	codgerente int,
    constraint FK_proyectos_EMP foreign key (codgerente) references empleados(codigo) -- defino la foreign key a nivel de tabla
);
select * from information_schema.REFERENTIAL_CONSTRAINTS where constraint_schema='pruebas' and table_name like 'proyectos'; -- Muestra las foreign keys ... y ahora sí aparece!!
-- Mysql permite definir foreign keys a nivel de columna, pero realmente no funcionan, por lo que hay que definirlas a nivel de tabla.

DESCRIBE empleados;

INSERT INTO empleados (apellidos,comision) VALUES ('Perez',20); --  me dará un error porque el nombre no puede ser nulo
INSERT INTO empleados (nombre,comision) VALUES ('Pedro',20); -- esto sí funciona porque el apellido sí puede ser nulo
SELECT * FROM empleados;

INSERT INTO departamentos(departamento,codjefe) values('ventas',1);


CREATE TABLE nuevosEmpleados AS SELECT * FROM empleados;
SELECT * FROM nuevosEmpleados; -- copia todos los datos
DESCRIBE nuevosEmpleados; -- pero pierde restricciones, como la primary key


CREATE TABLE nuevosEmpleados1 LIKE empleados;
DESCRIBE nuevosEmpleados1; -- copia la estructura
SELECT * FROM nuevosEmpleados1; -- pero pierde los datos


ALTER TABLE empleados ADD numhijos TINYINT; 
ALTER TABLE empleados drop numhijos;
ALTER TABLE empleados ADD numhijos TINYINT not null default NULL; -- si intento hacer algo así me dará un error por asignar un valor erróneo
ALTER TABLE empleados ADD numhijos TINYINT not null default 0; 
DESCRIBE empleados;

ALTER TABLE empleados add coddepto int;

ALTER TABLE empleados add constraint fk_emp_coddepto foreign key (coddepto) references departamentos(coddepto);
-- al definir la FK sobre una sola columna podía haberlo hecho cuando creé el campo: ALTER TABLE empleados add coddepto int references departamentos(coddepto);

ALTER TABLE empleados CHANGE codigo identificador TINYINT; -- le cambio el nombre y el tipo de datos: pasa de INT auto_increment a TINYINT -> me da un error porque tengo una FK en proyectos que apunta a ese campo
alter table proyectos drop foreign key FK_proyectos_EMP; -- borro la FK
ALTER TABLE empleados CHANGE codigo identificador TINYINT; -- ahora sí
DESCRIBE empleados;

ALTER TABLE empleados modify identificador INT auto_increment; -- para cambiar solo el tipo, uso modify en lugar de change
DESCRIBE empleados;

ALTER TABLE empleados CHANGE identificador cod int auto_increment;
DESCRIBE empleados;

RENAME TABLE  nuevosEmpleados TO newEmpleados;

ALTER TABLE nuevosEmpleados1 RENAME TO oldEmpleados;

alter table empleados add constraint ch_emp_coddepto check (coddepto>1); -- las restricciones check no existen en mysql: permite crearlas pero no funcionan
insert into empleados (nombre, apellidos,comision,coddepto) values ('José','Rodriguez',150,1); -- permite hacerlo

/* INDICES */
show indexes from departamentos; -- en cada primary key (codepto) se crea automáticamente un índice. Las columnas unique (departamento) se implementan también por medio de un índice. 
show indexes from empleados;  -- en cada FK se crea también automáticamente un índice
create index idx_emp_nombre on empleados(nombre);
drop index idx_emp_nombre on empleados; -- lo borramos
create unique index idx_emp_nombre on empleados(nombre); -- lo creamos como unico. 
insert into empleados(nombre,apellidos,comision,coddepto,numhijos) values ('Rebeca','Rodriguez',200,1,0); -- al insertar de nuevo el nombre "Rebeca" dará un fallo
insert into empleados(nombre,apellidos,comision,coddepto,numhijos) values ('Rebeca','Perez',200,1,0); -- al insertar de nuevo el nombre "Rebeca" dará un fallo
drop index idx_emp_nombre on empleados;
create unique index idx_emp_nombre on empleados(nombre, apellidos); -- la diferencia entre una columna unique y un índice unique es que el índice puede englobar varias columnas
insert into empleados(nombre,apellidos,comision,coddepto,numhijos) values ('Rebeca','Perez',200,1,0); -- como cambiamos el apellidos de Rebeca, da no da error
create index idx_emp_hijosemp on empleados(numhijos,nombre);

/* VISTAS */
create view v_empleados as select * from empleados where coddepto=1; -- vista horizontal
desc v_empleados;
insert into departamentos (departamento, codjefe) values ('ventas',1);
insert into empleados(nombre,apellidos,comision,coddepto,numhijos) values ('Rebeca','Lopez',200,1,0);
select * from v_empleados; -- la vista es una consulta: si inserto nuevos datos en la tabla, aparecen en la vista

create or replace view v_empleados (codemp,nom, comis, numdepto) as select cod,nombre, comision,coddepto from empleados; -- vista vertical. Puedo cambiar los nombres de las columnas que devuelve la vista
desc v_empleados;

create or replace view v_empleados_mixta as select cod,nombre,comision from empleados where coddepto=1; -- uso los nombres que devuelve la consulta para las columnas de la vista
desc v_empleados_mixta;

create or replace view v_empleados_depto as select e.cod, e.nombre, d.coddepto,d.departamento from empleados e, departamentos d where e.coddepto=d.coddepto; -- vista sobre varias tablas

SELECT * FROM information_schema.views; -- muestra todas las vistas de la BD

drop view v_empleados_depto;

/* TRUNCATE */
truncate table empleados;
select * from empleados;

DROP TABLE empleados;









