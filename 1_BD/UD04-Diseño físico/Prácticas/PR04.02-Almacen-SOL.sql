drop database if exists PR0402;
create database PR0402;
use PR0402;

create table producto(
	codprod int unsigned auto_increment primary key,
    ean char(12) unique not null,
    producto varchar(100) not null
);

create table precios(
	codprod int unsigned,
    fecha date,
    precio decimal(10,2) not null,
    constraint PK_precios primary key (codprod,fecha),
    constraint FK_precios_prod foreign key (codprod) references producto (codprod) on delete cascade on update cascade
);

create table cliente(
	codcli mediumint unsigned auto_increment primary key,
    cc char(20) unique not null,
    nif char(9) unique not null,
    tipocliente enum('particular','almacen','farmacia','otros') not null,
    nombre varchar(50),
    apellido varchar(100),
    denominacion varchar(150)
);

create table tfno_cli(
	codcli mediumint unsigned,
    telefono char(9),
    constraint PF_tfnocli primary key (codcli,telefono),
    constraint FK_tfnocli_cli foreign key (codcli) references cliente(codcli)
);

create table furgon(
	matricula char(7) primary key
);

create table empconductor(
	codemp tinyint unsigned auto_increment primary key,
    nombre varchar(50) not null,
    apellidos varchar(100) not null,
    cc char(20) unique not null,
    direccion varchar(150) not null,
    tipocarnet set ('A1','A2','B1','B2') default 'B1' not null
);

create table pedido(
	codpedido serial primary key,
    fecha date not null,
    hora time not null,
    descuento decimal(5,2) not null,
    tipopedido enum('online','telefonico','interno') not null,
    ip varchar(15),
    codcli  mediumint unsigned not null,
    codconductor tinyint unsigned,
    codfurgon char(7),
    fecenvio timestamp,
    constraint FK_pedido_cli foreign key(codcli) references cliente(codcli) on delete restrict on update cascade,
    constraint FK_pedido_conductor foreign key (codconductor) references empconductor(codemp) on delete set null on update cascade,
    constraint FK_pedido_furgon foreign key (codfurgon) references furgon(matricula) on delete restrict on update cascade
);

create table linpedido(
	codpedido bigint unsigned,
    codprod int unsigned,
    unidades tinyint unsigned not null,
    precio decimal(12,2) not null,
    constraint PF_linpedido primary key (codpedido,codprod),
    constraint FK_linpedido_prod foreign key (codprod) references producto(codprod) on delete restrict on update cascade,
    constraint FK_linpedido_ped foreign key(codpedido) references pedido(codpedido) on delete restrict on update cascade
);



select sum(case when data_type like '%int%' or data_type='year' then numeric_precision else 0 end) as NUM,sum(case when data_type like '%char%' or data_type like '%text%' then 1 else 0 end) as TXT,
sum(case when data_type ='enum' then character_maximum_length else 0 end) as ENU,sum(case when data_type ='set' then character_maximum_length else 0 end) as CONJ,
sum(case when is_nullable='YES' then 1 else 0 end) as NUL,sum(case when data_type ='timestamp' then 0.5 when 'datetime' then 1 else 0 end) as FEC,count(*) as columnas,
sum(case when column_default is not null then 1 when extra='auto_increment' then 0.1 else 0 end) as defectos
from information_schema.columns where table_schema='PR0402';

-- NUM	TXT	ENU	CONJ	NUL	FEC	columnas	defectos
-- 100	15	20	11		7	0.5	36			1.4

select count(*) as columnas, sum(case when cons.constraint_type='PRIMARY KEY' then 1 else 0 end) as PKs,
 sum(case when cons.constraint_type='FOREIGN KEY' then 1 else 0 end) as FKs,
 sum(case when cons.constraint_type='UNIQUE' then 1 else 0 end) as UQs
from information_schema.KEY_COLUMN_USAGE uso,information_schema.TABLE_CONSTRAINTS cons
where uso.constraint_schema='PR0402' and uso.constraint_name=cons.constraint_name
and uso.table_name=cons.table_name
and uso.constraint_schema=cons.constraint_schema;
-- columnas	PKs	FKs	UQs
-- 23		11	7	5

-- Cambia el nombre del campo “codcli” de la tabla Pedido por “codcliente”.
alter table pedido change codcli codcliente mediumint unsigned not null;
-- Cambia también el nombre de la tabla “EmpConductor” por “Conductor”.
alter table empconductor rename to conductor;
-- En el campo fecenvio de la tabla Pedido nos llega con saber la fecha, por lo que queremos cambiar el tipo de dato para que no guarde la hora.
alter table pedido modify fecenvio date;
-- Divide el campo “apellido” del cliente en dos: apellido1 y apellido2.
alter table cliente change apellido apellido1 varchar(100);
alter table cliente add apellido2 varchar(100);
-- La dirección de los conductores podrá ser nula.
alter table conductor modify direccion varchar(150);
-- Queremos que las unidades de la tabla Linpedido se muestren siempre con 3 dígitos (si tuviesen menos, deberá rellenarse automáticamente con ceros a la izquierda).
alter table linpedido modify unidades tinyint unsigned zerofill not null;
-- La limitación de 100 caracteres para el nombre del producto se ha quedado corta.  Incrementa el campo para que permita guardar 150 caracteres.
alter table producto modify producto varchar(150) not null;
-- Necesitamos limitar el acceso para el personal de administración de modo que solo puedan consultar la siguiente información de los clientes: nombre, cc y nif. 
create or replace view v_lim_clientes as select nombre, cc, nif from cliente;
-- Siempre que consultamos un cliente, lo hacemos por su nombre y tipo de cliente, por lo que nos interesaría que estas consultas se ejecuten lo más rápidamente posible.
create index idx_cliente_nbtipo on cliente(nombre, tipocliente);
-- Queremos que se puedan realizar rápidamente consultas sobre la tabla de conductores en base a su dirección, que además sabemos que será única. 
create unique index idx_conductor_dir on conductor(direccion);
-- Elimina el campo “cc” de la tabla de conductores.
alter table conductor drop cc;

select sum(case when column_name like 'cod%nte' then 1 else 0 end) as COL, sum(case when data_type ='date' then 1 else 0 end) as FEC, sum(case when is_nullable='YES' then 1 else 0 end) as NUL, sum(case when column_type like '%fill' then 1 else 0 end) as NUM, count(*) as columnas
from information_schema.columns where table_schema='PR0402';
-- COL	FEC	NUL	NUM	columnas
-- 1	3	10	1	39
select sum(case when lower(table_name) like 'con%ctor' then 0.5 else 1 end) as tablas,
sum(case when table_type='VIEW' then 1 else 0 end) as vista
from information_schema.tables tab
where tab.table_schema='PR0402';
-- tablas	vista
-- 8.5		1
select sum(case when non_unique=0 then 0.1 else 1 end) as unicos, sum(case when seq_in_index>1 then 1 else 0 end) as compuestos
FROM INFORMATION_SCHEMA.STATISTICS
where table_schema='PR0402';
-- unicos	compuestos
-- 7.6		4


