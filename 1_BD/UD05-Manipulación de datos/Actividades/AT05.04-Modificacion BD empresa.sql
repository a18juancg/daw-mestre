use empresa;
-- 1. Añade un nuevo producto de nombre PRODUCTO_PROBA, con código de fabricante ASU, identificador de producto ABCDE y con el mismo precio y existencias que el producto con descripción 3D PRO JOYSTICK.
desc produto;
insert into produto (cod_fabricante,identificador,descricion,prezo,existencias)
select 'ASU','ABCDE','PRODUCTO_PROBA',prezo,existencias from produto where descricion='3D PRO JOYSTICK';
select * from produto;
-- 2. Añade un nuevo fabricante a la BD con código AAA y el mismo nombre que el fabricante del pedido 10600, pero cambiándole las letras E por A (Por ejemplo, si el fabricante fuese SEAGATE, el fabricante nuevo tendría por nombre SAAGATA).
desc fabricante;
insert into fabricante(codigo,nome)
select 'AAA',replace(f.nome,'E','A') from fabricante f, pedido p where f.codigo=p.cod_fabricante and p.numero=10600;
-- 3. Inserta con una sola operación los siguientes valores en la tabla FABRICANTE: ('ASU','ASUS'), ('DEL','DELL'). En caso de que la clave de algún registro ya exista en la tabla, no se debe insertar el registro.
insert ignore into fabricante(codigo,nome) values
('ASU','ASUS'), ('DEL','DELL');
-- 4. Inserta con una sola operación los siguientes valores en la tabla FABRICANTE: ('TOS','TOSSSHIBA'), ('LEN','LENOVO'). En caso de que la clave de algún registro ya exista en la tabla, se deben actualizar los datos.
insert into fabricante(codigo,nome)
values ('TOS','TOSSSHIBA'), ('LEN','LENOVO')
on duplicate key update nome=values(nome);
-- 5. Inserta en la tabla FABRICANTE la fila ('KIN', 'KINGSTON'). En caso de que la clave ya exista en la tabla debe ponerle el nombre 'THE KING'.
insert into fabricante(codigo,nome)
values ('KIN', 'KINGSTON') 
on duplicate key update nome='THE KING';
-- 6. Inserta en la tabla de empleados los datos del fichero “AT05.04-Empleados.txt”.
LOAD DATA INFILE "C:\\ProgramData/MySQL/MySQL Server 8.0/Uploads/AT05.04-Empleados.txt" 
INTO TABLE EMPREGADO
FIELDS TERMINATED BY '€'
enclosed by '"'
LINES TERMINATED BY '\r\n'
IGNORE 2 LINES
(numero, nome, ape1, ape2, data_nacemento, id_sucursal_traballa, titulo, data_contrato, num_empregado_xefe, cota_de_vendas);
select * from empregado;
-- 7. Con una sola instrucción, divide la cuota de ventas de todos los empleados de la sucursal de Valencia entre 7 y divide también entre 7 el objetivo de la propia sucursal.
update empregado e, sucursal s
set e.cota_de_vendas=e.cota_de_vendas/7,s.obxectivo=s.obxectivo/7
where e.id_sucursal_traballa=s.identificador
and s.cidade ='Valencia';
-- 8. Modifica todos los empleados que hayan quedado con una cuota de ventas decimal (no entera) para asignarles el entero más alto anterior a su cuota (por ejemplo, si tienen cuota 205.8 deberían pasar a tener 205).
update empregado
set cota_de_vendas=floor(cota_de_vendas) -- truncate(cota_de_vendas,0)
where cota_de_vendas!=truncate(cota_de_vendas,0);
-- 9. Incrementa en un mes la fecha de nacimiento de todos los empleados.
update empregado
set data_nacemento=date_add(data_nacemento,interval 1 month);
-- 10. Inserta los siguientes productos (con los espacios que se indica en la descripción):
-- cod_fabricante identificador descricion prezo existencias
-- SAM 11004 ‘Webcam S270 ‘ 32.37 5
-- SAM 1100X ‘ Unidad USB’ 51.4 4
-- SAM 1100Y ‘ Joystick 4D JK93 ‘ 50.87 6
insert into produto ( cod_fabricante,identificador,descricion,prezo,existencias) values
('SAM','11004','Webcam S270 ',32.37,5),
('SAM','1100X',' Unidad USB',51.4,4),
('SAM','1100Y',' Joystick 4D JK93 ',50.87, 6);
-- 11. Elimina los espacios que hayan quedado al inicio o al final de la descripción de los productos solo en los que tengan espacios en blanco al principio.
update produto
set descricion=trim(descricion)
where descricion!=ltrim(descricion);
-- 12. Divide entre 3 el límite de crédito de los clientes cuyo nombre contenga la cadena ‘INFO’ y acabe por A, rendondeando el valor a 2 decimales.
update cliente 
set limite_de_credito=round(limite_de_credito/3,2)
where nome like '%INFO%A';
-- 13. Incrementa el objetivo de las sucursales de la región OESTE en un 6%, y modificar el nombre de la región por WEST.
update sucursal
set obxectivo=obxectivo*1.06,rexion='WEST'
where rexion='OESTE';
-- 14. Crea una tabla “CONSULTAS” con los campos numemp y numcli para guardar las consultas realizadas por los clientes a los empleados. Para probar su funcionamiento, inserta 4 filas en la tabla con valores aleatorios (tanto de empleados como de clientes) comprendidos entre 0 y 2000 en el caso de los clientes, y entre 0 y 200 en el caso de los empleados.
create table consultas (numemp int, numcli int);

insert into consultas(numemp,numcli) values
(truncatE(rand()*200,0),truncatE(rand()*2000,0)),
(truncatE(rand()*200,0),truncatE(rand()*2000,0)),
(truncatE(rand()*200,0),truncatE(rand()*2000,0)),
(truncatE(rand()*200,0),truncatE(rand()*2000,0));

insert into consultas (numemp,numcli)
select truncatE(rand()*2000,0),truncatE(rand()*200,0)
union
select truncatE(rand()*2000,0),truncatE(rand()*200,0)
union
select truncatE(rand()*2000,0),truncatE(rand()*200,0)
union
select truncatE(rand()*2000,0),truncatE(rand()*200,0);
select * from consultas;

-- 15. Transferir todos los empleados que trabajan en la sucursal de BARCELONA a la sucursal de VIGO, y cambiar su cuota de ventas por la media de las cuotas de ventas de todos los empleados.
update empregado
set id_sucursal_traballa=(select identificador from sucursal where cidade='VIGO'),
	cota_de_vendas=(select avg(q.cota_de_vendas) from (select * from empregado) as Q)
where id_sucursal_traballa in (select identificador from sucursal where cidade='BARCELONA');
-- 16. Crea una tabla CLIENTES_101 con los clientes asignados al empleado 101, y una tabla CLIENTES_VIP con los clientes con límite de crédito mayor a 50.000€. Elimina simultáneamente de ambas tablas los clientes que estén en las dos a la vez.
create table clientes_101 as select * from cliente where num_empregado_asignado=101;
create table clientes_vip as select * from cliente where limite_de_credito>50000;

delete c1,c2
from clientes_101 c1, clientes_vip c2
where c1.numero=c2.numero;

-- 17. Elimina los fabricantes que no suministren ningún producto.
delete from fabricante where codigo not in (select cod_fabricante from produto);
-- 18. Elimina los pedidos de empleados contratados antes del año 2001.
delete from pedido where num_empregado in (select numero from empregado where data_contrato<'2001-01-01');
delete from pedido where num_empregado in (select numero from empregado where year(data_contrato)<2001);
-- 19. Crea una tabla de nombre FABRICANTE2 que sea una copia de FABRICANTE en estructura y contenido. Elimina de FABRICANTE2 todos los fabricantes cuyo nombre empiece por ‘S’.
create table fabricante2 like fabricante;
insert into fabricante2 select * from fabricante;
delete from fabricante2 where nome like 'S%';
-- 20. Elimina de FABRICANTE2 todos los registros excepto ASUS, TOSHIBA y WESTERN DIGITAL
delete from fabricante2 where nome not in ('ASUS', 'TOSSSHIBA', 'WESTERN DIGITAL');
-- 21. Elimina todas las filas de la tabla FABRICANTE2 de la forma más rápida y menos costosa posible.
truncate fabricante2;
