use empresa;
-- 1. Nombre de todos los clientes por orden alfabético.
select nome from cliente order by 1;
-- 2. Nombre de las regiones en las que tiene sucursales la empresa.
select distinct rexion from sucursal;
-- 3. Identificador de los productos que nos pidieron en algún momento. En el resultado debe aparecer en una sola columna el código del fabricante y el identificador del producto, separados por un guión. La columna del resultado deberá llamarse productos.
select distinct concat(pr.cod_fabricante,'-',pr.identificador) as producto
from produto pr,pedido ped
where pr.identificador=ped.id_produto and pr.cod_fabricante=ped.cod_fabricante;
-- 4. Información completa de las sucursales no dirigidas por el empleado número 108.
select * from sucursal where num_empregado_director!=108;
-- 5. Nombre y límite de crédito del cliente número 1107.
select nome,limite_de_credito from cliente where numero=1107;
-- 6. Número y fecha de los pedidos hechos entre el 1 de agosto y el 31 de diciembre de 2014. Solo debe aparecer la fecha de cada pedido, sin la hora, con formato dd-mm-aaaa. Deben aparecer primero en el resultado los pedidos más recientes. Para resolver esta consulta no se pueden utilizar operadores de comparación (>, <, >=,<=, < >, !=).
select numero,date_format(date(data_pedido),'%d-%m-%Y')
from pedido
where data_pedido between '2014-08-01' and '2014-12-31 23:59'
order by data_pedido desc;
-- 7. Código y nombre de los fabricantes cuyo nombre tiene por segunda letra O.
select codigo,nome from fabricante where nome like '_O%';
select codigo,nome from fabricante where right(left(nome,2),1)='O';
-- 8. Descripción y precio de los productos de los que no tenemos existencias.
select descricion,prezo from produto where existencias=0;
-- 9. Número identificador y nombre completo de los empleados que no tienen jefe.
select numero,concat(nome,' ',ape1,' ',ifnull(ape2,'')) as empleado 
from empregado 
where num_empregado_xefe is null;
-- 10. Descripción y unidades existentes, de los productos con existencias mayores de 10 unidades y menores de 100. Para resolver esta consulta no se pueden utilizar operadores de comparación (>, <, >=, <=, < >, !=).
select descricion,existencias from produto where existencias between 11 and 99;
-- 11. Consulta que devuelva el código y nombre de los fabricantes cuyo nombre no tiene por segunda letra O.
select codigo,nome from fabricante where nome not like '_O%';
select codigo,nome from fabricante where not right(left(nome,2),1)='O';
-- 12. Número identificador, nombre completo y fecha de nacimiento de los empleados que trabajan en la sucursal con identificador 12, y nacieron en el año 1985. En el resultado aparecerán primero los empleados más nuevos.
select numero, concat(nome,' ',ape1,' ',ifnull(ape2,'')) as empleado,data_nacemento from empregado
where id_sucursal_traballa=12 and year(data_nacemento)=1985
order by data_contrato desc;
-- 13. Número de pedido de aquellos en los que se pidieron 6 ó 10 unidades. Para resolver esta consulta no se pueden utilizar operadores de comparación (>, <, >=, <=, < >, !=,=).
select numero,cantidade from pedido where cantidade in (6,10);
-- 14. Número y nombre propio (en una única columna separados por un guión, número - nombre_propio) de los empleados que no tienen segundo apellido. La columna del resultado deberá llamarse datos_empleados.
select numero,concat(nome,' ',ape1) as datos_empleados 
from empregado
where ape2 is null;
-- where length(trim(ifnull(ape2,'')))=0;
-- 15. Empleando subconsultas, realizar una listado del código del fabricante e identificador de aquellos productos con precio superior a 60€ o que tengan pedidos de cantidad inferior a 5 unidades. El resultado aparecerá ordenado por fabricante y para el mismo fabricante, por producto.
select cod_fabricante,identificador ,prezo
from produto pr
where prezo>60 or 
-- (cod_fabricante,identificador) in (select cod_fabricante,id_produto from pedido where cantidade<5);
exists(select 1 from pedido ped where ped.cod_fabricante=pr.cod_fabricante and ped.id_produto=pr.identificador and cantidade<5);
-- 16. Empleando subconsultas, mostrar los códigos de los empleados que no hicieron pedidos. Deberán aparecer primero los empleados con código mayor.
select numero from empregado 
where numero not in (select num_empregado from pedido)
order by numero desc;
-- 17. Empleando subconsultas, mostrar el código de los clientes que hicieron pedidos y con límite de crédito mayor o igual a 40000.
select numero from cliente
where limite_de_credito>=40000 and numero in (select num_cliente from pedido);

/*19. Empleando subconsultas, mostrar el código de los empleados que son directores de alguna sucursal o que tienen una cuota de ventas superior a 250000€. Debes proponer dos soluciones:
- en la primera solo puede aparecer una vez cada empleado en el resultado
- en la segunda, si un empleado es director de una sucursal y además tiene una cuota superior a 250000€,aparecerá en el resultado más de una vez. */
select num_empregado_director from sucursal
union
select numero from empregado where cota_de_vendas>250000;
select num_empregado_director from sucursal
union all
select numero from empregado where cota_de_vendas>250000
order by 1;
-- 20. Empleando subconsultas, mostrar el número identificador de los clientes que aún no hicieron pedidos. Ordena el resultado por el número de cliente en orden ascendente.
select numero from cliente where numero not in (select num_cliente from pedido) order by 1 asc;
-- 21. Nombre de todos los fabricantes de los que se hicieron pedidos. Debes proponer dos soluciones, una con la sintaxis con la condición de combinación en el WHERE, y otra con la sintaxis con la condición de combinación en el FROM.
select distinct f.nome 
from fabricante f,pedido p
where f.codigo=p.cod_fabricante;
select distinct f.nome 
from fabricante f join pedido p on (f.codigo=p.cod_fabricante);
-- 22. Nombre de todos los fabricantes, se le hicieran o no pedidos. Si tuvieron pedidos aparecerá el nombre y en una segunda columna el número de pedido. Si de un fabricante se hicieron más de un pedido, aparecerá tantas veces como pedidos se le hicieron. En caso de no tener pedidos, como número de pedido deberá aparecer el valor 99.
select f.nome,ifnull(p.numero,99)
from fabricante f left outer join pedido p on (f.codigo=p.cod_fabricante);
-- 23. Repite la consulta anterior, pero en lugar de mostrar 99 como número de pedido en los fabricantes sin pedido, muestra la cadena ‘Sin pedidos’
select f.nome,ifnull(p.numero,'sin pedidos')
from fabricante f left outer join pedido p on (f.codigo=p.cod_fabricante);
-- 24. Código de los productos (con formato cod_fabricante-id_producto) y descripción, de los productos que no fueron pedidos nunca.
select concat(cod_fabricante,'-',identificador) as producto,descricion
from produto pr 
-- where (cod_fabricante,identificador) not in (select cod_fabricante,id_produto from pedido);
where not existS(select 1 from pedido p where p.cod_fabricante=pr.cod_fabricante and p.id_produto=pr.identificador);
-- como curiosidad, también se podría hacer así:
select concat(pr.cod_fabricante,'-',identificador) as producto,descricion
from produto pr left outer join pedido p on (p.cod_fabricante=pr.cod_fabricante and p.id_produto=pr.identificador)
where p.numero is null;
-- taran!!
-- 25. Producto cartesiano entre la tabla de sucursales y la de empleados. En una primera columna aparecerá la ciudad de la sucursal y en la segunda el nombre completo del empleado (con formato nombre ape1 ape2).
select cidade,concat(nome,' ',ape1,ifnull(concat(' ',ape2),''))
from sucursal,empregado;
-- 26. Número y nombre completo (con formato nombre ape1 ape2) de todos los empleados, así como la ciudad de la sucursal que dirigen, si es que dirigen alguna. En la tercera columna, de nombre sucursal_que_dirige, en las filas de los empleados que no son directores de sucursales, deberá aparecer la frase 'No es director.'.
select numero,concat(nome,' ',ape1,ifnull(concat(' ',ape2),'')) as empleado,ifnull(cidade,'No es director') as sucursal_que_dirige
from empregado e left outer join sucursal s
on (e.numero=s.num_empregado_director);
-- 27. Número y nombre completo de los empleados que tienen jefe, con número y nombre completo del jefe en una segunda columna. En ambas columnas aparecerá el número separado del nombre completo por un guión.
select concat(e.numero,'-',e.nome) as empleado,concat(jefe.numero,'-',jefe.nome) as jefe
from empregado e,empregado jefe
where e.num_empregado_xefe=jefe.numero;
-- 28. Igual al anterior, pero si algún empleado no tuviese jefe, en la segunda columna debe aparecer la frase 'Jefe por designar.'.
select concat(e.numero,'-',e.nome) as empleado,ifnull(concat(jefe.numero,'-',jefe.nome),'Jefe por designar') as jefe
from empregado e left outer join empregado jefe on (e.num_empregado_xefe=jefe.numero);
-- 29. Nombre completo de todos los empleados con el nombre del cliente que tienen asignado. En caso de que no tuviesen ningún cliente asignado aparecerá en el nombre del cliente la frase 'Sin cliente.'. De la misma forma, si un cliente no tiene empleado asignado, en la columna del empleado aparecerá 'Sin vendedor.'. Es importante que aparezcan todos los empleados, tengan o no clientes y todos los clientes tengan o no empleados.
select concat(e.nome,' ',e.ape1,ifnull(concat(' ',e.ape2),'')) as empleado,ifnull(cl.nome,'sin cliente') as cliente
from empregado e left outer join cliente cl on (cl.num_empregado_asignado=e.numero)
union
select ifnull(concat(e.nome,' ',e.ape1,ifnull(concat(' ',e.ape2),'')),'sin empleado') as empleado,cl.nome as cliente
from empregado e right outer join cliente cl on (cl.num_empregado_asignado=e.numero);
-- 30. Listado de los productos de la BD ordenados alfabéticamente por descripción. En el resultado aparecerán dos columnas, en la primera el nombre del fabricante y en la segunda la descripción del producto. Las columnas se llamarán Fabricante y Producto. Se deberán proponer dos soluciones, una primera con la condición de combinación en la cláusula FROM (usando JOIN), y una segunda con la condición de combinación en la cláusula WHERE (sin join, cruzando las columnas en el where).
select f.nome as fabricante,p.descricion as producto
from fabricante f,produto p
where f.codigo=p.cod_fabricante;
select f.nome as fabricante,p.descricion as producto
from fabricante f join produto p on (f.codigo=p.cod_fabricante);
-- 31. Media de unidades vendidas de cada vendedor. El resultado tendrá dos columnas: en la primera el número identificador del empleado (vendedor) y en la segunda columna la media de unidades vendidas (campo cantidad) de sus pedidos.
select num_empregado,avg(cantidade) as media_unidades
from pedido
group by num_empregado;
-- 32. Precio más barato de los productos, precio más caro, precio medio, suma total de los precios de los productos y número de productos distintos existentes.
select min(prezo),max(prezo),avg(prezo),sum(prezo),count(*)
from produto;
-- 33. Número de pedidos realizados por el cliente 1103.
select count(numero) from pedido where num_cliente=1103;
-- 34. Número de pedidos realizados por cada cliente. En el resultado aparecerá el identificador del cliente y en la segunda columna el número de pedidos que lleva hechos cada cliente hasta ahora.
select num_cliente,count(numero) 
from pedido
group by num_cliente;
-- 35. Repite la consulta anterior, pero ahora en el resultado solo podrán aparecer los clientes que hicieron más de 2 pedidos.
select num_cliente,count(numero) 
from pedido
group by num_cliente
having count(numero)>2;
-- 36. Repite la consulta anterior, pero ahora en el resultado solo podrán aparecer los clientes que hicieron más de 2 pedidos y que además tienen una media de unidades compradas (cantidad) inferior a 10.
select num_cliente,count(numero) 
from pedido
group by num_cliente
having count(numero)>2 and avg(cantidade)<10;
-- 37. Cantidad total de sucursales que hay por región. Aparecerá el nombre de la región y en la misma columna separado por un guión, la cantidad de sucursales situadas en esa región.
select concat(rexion,'-',count(identificador))
from sucursal
group by rexion;
select count(*) from sucursal
group by rexion;
-- 38. Cantidad total de empleados que hay por sucursal. Aparecerá el identificador de la sucursal y en una segunda columna la cantidad de empleados que trabajan en la misma.
select id_sucursal_traballa,count(*) from empregado
group by id_sucursal_traballa;
-- 39. Precio medio de los productos por fabricante. En una primera columna aparecerá el código de tres caracteres del fabricante, y en la segunda el precio medio de los productos de ese fabricante. En el resultado deberán aparecer primero los fabricantes que tengan el mayor precio medio en cada producto.
select cod_fabricante,count(identificador)
from produto
group by cod_fabricante
order by 2 desc;
-- 40. Repite la consulta anterior, pero ahora solo pueden aparecer los fabricantes con más de tres productos a la venta.
select cod_fabricante,count(identificador)
from produto
group by cod_fabricante
having count(identificador)>3
order by 2 desc;
-- 41. Cantidad de empleados que son directores de alguna sucursal. Ten en cuenta que distintas sucursales pueden tener el mismo director.
select count(distinct num_empregado_director) from sucursal;
-- 42. Nombre de todos los fabricantes de los que hay productos en la BD, sin usar subconsultas.
select distinct f.nome
from fabricante f,produto p
where f.codigo=p.cod_fabricante;
-- 43. Nombre de todos los fabricantes de los que no hay productos en la BD, sin usar subconsultas.
select distinct f.nome
from fabricante f left outer join produto p on (f.codigo=p.cod_fabricante)
where p.identificador is null;
-- 44. Número de pedido, cantidad y fecha de pedido para aquellos pedidos recibidos en los días en que un nuevo empleado fue contratado.
select pedido.numero, cantidade, data_pedido
from pedido, empregado
where date(pedido.data_pedido) = empregado.data_contrato;
update empregado set data_contrato='2014-03-02' where data_contrato='2014-03-01';
-- 45. Ciudad y objetivo de las sucursales cuyo objetivo supera la media de las cuotas de todos los vendedores de la BD.
select cidade, obxectivo
from sucursal
where obxectivo > (select avg(cota_de_vendas) from empregado);
-- 46. Número de empleado y cantidad media de los pedidos de aquellos empleados cuya cantidad media de pedido es superior a la cantidad media global (de todos los pedidos).
select empregado.numero, avg(cantidade)
from empregado, pedido
where empregado.numero = pedido.num_empregado 
group by empregado.numero
having avg(cantidade) > (select avg(cantidade) from pedido);
-- 47. Nombre de los clientes que aún no hicieron pedidos, sin usar subconsultas.
select distinct nome, pe.numero
from cliente cl left outer join pedido pe on cl.numero = pe.num_cliente
where pe.numero is null;

-- 48. Nombre completo de los empleados cuyas cuotas son iguales o superiores al objetivo de la sucursal de la ciudad de Vigo. Ten en cuenta que si la cuota de un vendedor (empleado) es nula debemos considerarla como un 0, y del mismo modo actuaremos con el objetivo de la sucursal.
select nome, ape1, ape2
from empregado
where ifnull(cota_de_vendas, 0) >= (select ifnull(obxectivo, 0)from sucursal where cidade = 'vigo');

-- 49. Nombre de los productos que tengan algún pedido de al menos 20 unidades. Hay que recordar que la identificación de un producto se hace por la combinación del código del fabricante y el del producto. La solución deberá hacerse empleando el predicado EXISTS con una subconsulta correlacionada.
select descricion
from produto pr
where exists(select 1 from pedido pe where pr.cod_fabricante = pe.cod_fabricante and pr.identificador = pe.id_produto and cantidade > 20);
-- 50. Ciudades de las sucursales con algún empleado cuya cuota de ventas represente más del 80% del objetivo de la oficina donde trabaja. Para resolver esta consulta se deberá emplear una subconsulta con ANY.
select cidade 
from sucursal s
where obxectivo*0.8 < ANY(select cota_de_vendas from empregado e where s.identificador=e.id_sucursal_traballa);
-- 51. Nombre de los clientes que tengan asignado un empleado que trabaje en una sucursal de la región OESTE. No se pueden usar joins, solo subconsultas encadenadas.
select nome from cliente c
where num_empregado_asignado in (
	select numero from empregado 
    where id_sucursal_traballa in (
		select identificador from sucursal where rexion='OESTE'	
    )
);
-- 52. Número y fecha de los pedidos realizados por empleados sin cuota de ventas.
select p.numero,data_pedido
from pedido p, empregado e
where p.num_empregado=e.numero and e.cota_de_vendas is null;
-- 53. Nombre y apellidos de los empleados. En los que tengan el segundo apellido a NULL, debe figurar el texto ‘DESCONOCIDO’.
select nome,ape1, ifnull(ape2,'DESCONOCIDO') from empregado;
-- 54. Primer carácter del fabricante y dos últimos caracteres del código identificador de producto de los pedidos realizados hace menos de tres años.
select left(cod_fabricante,1),right(id_produto,2), data_pedido
from pedido 
where data_pedido> subdate(now(),INTERVAL 5 year);
-- 55. Nombre de los clientes en minúsculas y reemplazando cualquier aparición de la cadena “INFOR” por “OFI”, de los clientes que tienen asignado un número de empleado par o no tienen un número de empleado asignado.

select lower(replace(nome, 'INFOR', 'OFI'))
from cliente
where num_empregado_asignado % 2 = 0 or num_empregado_asignado is null;

-- 56. Fabricante, identificador, precio, precio redondeado sin decimales, valor entero del precio y siguiente entero al precio de todos los productos cuyo segundo decimal es un cero.

select cod_fabricante, identificador, prezo, round(prezo, 0), truncate(prezo, 0), ceil(prezo)
from produto
where right(truncate(prezo, 2), 1) = 0;
-- 57. Muestra un listado de todos los empleados de manera que el resultado aparezca en una sola columna con el siguiente formato: ‘El empleado [nombre] [apellido1] fue contratado en [mes de contratación] de [año de contratación]’.
select concat('El empleado ',nome,' ', ape1,' fue contratado en ',month(data_contrato),' de ',year(data_contrato))
from empregado;
-- 58. Muestra un listado de todos los empleados con su antigüedad (tiempo transcurrido desde su fecha de contratación hasta la fecha actual en años).
select nome, truncate(datediff(now(), data_contrato) / 365, 0)
from empregado;
-- 59. Muestra un campo “empleado” que se calculará a partir de la tabla de empleados de la siguiente manera: si el empleado nació antes de 1970 mostrará su primer apellido, si nació entre 1970 y 1980 (incluidos) mostrará sus dos apellidos, y si nació después de 1980 mostrará su nombre.
select case 
			when year(data_nacemento)<1970 then ape1 
			when year(data_nacemento) between 1970 and 1980 then concat(ape1,ifnull(concat(' ',ape2),'')) 
			else nome 
		end as empleado
from  empregado;
/* 60. Consulta que devuelva la siguiente información de los empleados:
- número identificador y nombre propio separados por el símbolo #.
- Tres primeras letras del primer apellido.
- Tres últimas letras del primer apellido.
- Tercer y cuarto caracteres del primer apellido.
- Nombre propio en minúsculas.
- Nombre propio en mayúsculas.
- Título eliminándole los posibles espacios en blanco a la izquierda.
- Título eliminándole los posibles espacios en blanco a la derecha.
- Título eliminándole todos los espacios.
- Nombre propio substituyendo E por O. */
select concat(numero,'#',nome), left(ape1,3),right(ape1,3),right(left(ape1,4),2),lower(nome),upper(nome),ltrim(titulo),rtrim(titulo),trim(titulo), replace(nome,'E','O')
from empregado;
/* 61. Consulta que devuelva la siguiente información de los empleados:
- Nombre completo de los empleados con formato apellido1 apellido2, nombre.
- Cuota de ventas.
- Cuota de ventas aproximada por exceso (entero superior).
- Cuota de ventas aproximada por defecto (entero inferior).
- Cuota de ventas elevada al cuadrado.
- Cuota de ventas elevada al cubo.
- Raíz cuadrada de la cuota de ventas.
Todos los valores nulos del resultado deberán aparecer sustituidos por el valor 0. */
select concat(ape1,' ',ifnull(ape2,''),', ',nome),ifnull(cota_de_vendas,0),ceil(ifnull(cota_de_vendas,0)),floor(ifnull(cota_de_vendas,0)),pow(ifnull(cota_de_vendas,0),2),pow(ifnull(cota_de_vendas,0),3),sqrt(ifnull(cota_de_vendas,0))
from empregado;
/* 62. Consulta que devuelva la siguiente información de los pedidos:
- Número de pedido.
- Fecha actual.
- Fecha del pedido con formato dd-mm-aaaa.
- Día en que se hizo el pedido.
- Nombre del mes en que se hizo el pedido.
- Año en que se hizo el pedido.
- Meses que pasaron desde que se hizo el pedido.
- Fecha de pedido adelantada 4 años.
- Fecha de pedido retrasada 1 mes. */
select numero,now(),date_format(data_pedido,'%d-%m-%Y'),date_format(data_pedido,'%W'),date_format(data_pedido,'%M'),year(data_pedido),
datediff(now(),data_pedido)/30, adddate(data_pedido,interval 4 year),subdate(data_pedido,interval 1 month)
from pedido;